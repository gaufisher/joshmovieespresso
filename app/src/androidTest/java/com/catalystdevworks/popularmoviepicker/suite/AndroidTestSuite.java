package com.catalystdevworks.popularmoviepicker.suite;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 * Created by gfisher on 2/9/2016.
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({InstrumentationTestSuite.class,UnitTestSuite.class})
public class AndroidTestSuite {
}
