package com.catalystdevworks.popularmoviepicker.suite;

import com.catalystdevworks.popularmoviepicker.DetailsActivityTests;
import com.catalystdevworks.popularmoviepicker.MainActivityFavoriteMoviesTests;
import com.catalystdevworks.popularmoviepicker.MainActivityPopularMoviesTests;
import com.catalystdevworks.popularmoviepicker.MainActivityRatingMoviesTests;
import com.catalystdevworks.popularmoviepicker.MainActivityTests;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 * Created by gfisher on 2/9/2016.
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({MainActivityTests.class, DetailsActivityTests.class, MainActivityFavoriteMoviesTests.class, MainActivityPopularMoviesTests.class, MainActivityRatingMoviesTests.class})
public class InstrumentationTestSuite {}
