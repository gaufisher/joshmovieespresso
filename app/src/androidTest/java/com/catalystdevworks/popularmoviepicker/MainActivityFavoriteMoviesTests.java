package com.catalystdevworks.popularmoviepicker;

import android.support.test.InstrumentationRegistry;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.test.suitebuilder.annotation.LargeTest;

import com.catalystdevworks.popularmoviepicker.activities.MainActivity;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static com.catalystdevworks.popularmoviepicker.constants.Constants.DATABASE_NAME;
import static com.catalystdevworks.popularmoviepicker.constants.Constants.FAVORITES_SORTING_TEXT;
import static com.catalystdevworks.popularmoviepicker.constants.Constants.NO_FAVORITES_TEXT;
import static com.catalystdevworks.popularmoviepicker.constants.Constants.SORT_BY_TEXT_ID;
import static com.catalystdevworks.popularmoviepicker.helpers.EspressoHelpers.checkTextMatchesByID;
import static com.catalystdevworks.popularmoviepicker.helpers.EspressoHelpers.deleteDatabase;
import static com.catalystdevworks.popularmoviepicker.helpers.EspressoHelpers.favoriteFirstMovie;
import static com.catalystdevworks.popularmoviepicker.helpers.EspressoHelpers.showFavoritedMovies;

/**
 * Created by gfisher on 2/17/2016.
 */
@RunWith(AndroidJUnit4.class)
@LargeTest
public class MainActivityFavoriteMoviesTests {

    @Rule
    public ActivityTestRule<MainActivity> mActivityRule = new ActivityTestRule<MainActivity>(
            MainActivity.class);

    /**
     * This ensures there are no favorites at the start of testing
     */
    @Before
    public void setUp() {
        deleteDatabase();
        showFavoritedMovies();
    }

    @Test
    public void showFavoritesTextIsCorrectWithMoviePresent() {
        favoriteFirstMovie();
        showFavoritedMovies();
        final String expectedText = FAVORITES_SORTING_TEXT;
        checkTextMatchesByID(expectedText, SORT_BY_TEXT_ID);
    }

    @Test
    public void showFavoritesTextIsCorrectWithoutMoviesPresent() {
        final String expectedText = NO_FAVORITES_TEXT;
        checkTextMatchesByID(expectedText, SORT_BY_TEXT_ID);
    }

    @After
    public void tearDown() {
        deleteDatabase();
    }
}
