package com.catalystdevworks.popularmoviepicker.helpers.MainActivity;

import com.catalystdevworks.popularmoviepicker.helpers.BaseHelpers;

import static com.catalystdevworks.popularmoviepicker.constants.Constants.RATED_MENU_TEXT;

/**
 * This method contains methods specific to the main Activity when showing the highest rated movies.
 * Created by gfisher on 2/22/2016.
 */
public class MainActivityRatingHelpers extends BaseHelpers {

}
