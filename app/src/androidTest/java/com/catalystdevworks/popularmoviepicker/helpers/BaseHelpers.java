package com.catalystdevworks.popularmoviepicker.helpers;

import android.support.annotation.IdRes;
import android.support.test.InstrumentationRegistry;
import android.support.test.espresso.ViewInteraction;
import android.support.test.espresso.matcher.BoundedMatcher;
import android.support.v7.widget.Toolbar;

import org.hamcrest.Description;
import org.hamcrest.Matcher;

import static android.support.test.espresso.Espresso.onData;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.Espresso.openActionBarOverflowOrOptionsMenu;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.scrollTo;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.hasDescendant;
import static android.support.test.espresso.matcher.ViewMatchers.isAssignableFrom;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static com.catalystdevworks.popularmoviepicker.constants.Constants.DATABASE_NAME;
import static com.catalystdevworks.popularmoviepicker.constants.Constants.LIST_VIEW_ID;
import static org.hamcrest.Matchers.anything;
import static org.hamcrest.Matchers.is;

/**
 * This class contains generic methods that can be applied across multiple apps.
 * Created by gfisher on 2/22/2016.
 */
public abstract class BaseHelpers {

    public BaseHelpers(){};

    /**
     * Scrolls to a view with the given id
     * @param viewID R.id of the view you want to scroll to
     */
    protected void scrollToViewWithId(@IdRes int viewID) {
        onView(withId(viewID))
                .perform(scrollTo());
    }

    /**
     * This method allows you to click on a view by text
     * @param text The visible text that you want to click on
     */
    protected void tapViewWithText(String text) {
        onView(withText(text)).perform(click());
    }

    /**
     * Opens the action bar overflow menu
     */
    protected void openOverflowMenu() {
        openActionBarOverflowOrOptionsMenu(InstrumentationRegistry.getTargetContext());
    }

    /**
     * Allows text to be entered into a view with a given R.id
     * @param textToEnter Text you would like to enter
     * @param viewID target view's R.id
     */
    protected void enterTextToViewWithID(String textToEnter, @IdRes int viewID) {
        onView(withId(viewID))
                .perform(typeText(textToEnter));
    }

    /**
     * performs a click on a view with the given id
     * @param viewID target view's R.id
     */
    protected void tapViewWithID(@IdRes int viewID) {
        onView(withId(viewID))
                .perform(click());
    }

    /**
     * Checks that the target view contains the string provided
     * @param expected String with expected text for view
     * @param viewID target view's R.id
     */
    protected void checkTextMatchesByID(String expected, @IdRes int viewID) {
        onView(withId(viewID))
                .check(matches(withText(expected)));
    }

    /**
     * Checks that the nth child row is visible
     * @param row The row number you expect is visible
     * @param viewID the R.id of the parent view to the row.
     */
    protected void checkNthRowVisibleInParentWithId(int row, @IdRes int viewID) {
        onData(anything())
                .inAdapterView(withId(viewID))
                .atPosition(row - 1)
                .check(matches(isDisplayed()));
    }

    /**
     * Deletes the given database
     * @param database The String name of the database you wish to delete
     */
    protected void deleteDatabase(String database) {
        InstrumentationRegistry.getTargetContext().getApplicationContext().deleteDatabase(database);
    }

    /**
     * Clicks on the specified item in an adapter view with the provided ID.
     * @param row The row that you wish to click on
     * @param viewID the R.id of the adapter view.
     */
    protected void clickNthItemInAdapterView(int row, @IdRes int viewID) {
        onData(anything())
                .inAdapterView(withId(viewID))
                .atPosition(row)
                .perform(click());
    }

    /**
     * Test that checks whether a parent view has a descendant with the given ID.
     * @param parentId The R.id of the parent view
     * @param childId The R.id of the child view that should be present.
     */
    protected void checkViewHasDescendantWithId(@IdRes int parentId, @IdRes int childId) {
        onView(withId(parentId))
                .check(matches(hasDescendant(withId(childId))));
    }

    /**
     * Checks that the toolbar title matches the expected title
     * @param title Expected toolbar title
     * @return the view interaction
     */
    protected ViewInteraction matchToolbarTitle(CharSequence title) {
        return onView(isAssignableFrom(Toolbar.class)).check(matches(withToolbarTitle(is(title))));
    }

    /**
     * matches the expected toolbar title with the actual toolbar title
     * @param textMatcher matcher of the expected toolbar title
     * @return matcher
     */
    private Matcher<Object> withToolbarTitle(
            final Matcher<CharSequence> textMatcher) {
        return new BoundedMatcher<Object, Toolbar>(Toolbar.class) {
            @Override public boolean matchesSafely(Toolbar toolbar) {
                return textMatcher.matches(toolbar.getTitle());
            }
            @Override public void describeTo(Description description) {
                description.appendText("with toolbar title: ");
                textMatcher.describeTo(description);
            }
        };
    }
}
