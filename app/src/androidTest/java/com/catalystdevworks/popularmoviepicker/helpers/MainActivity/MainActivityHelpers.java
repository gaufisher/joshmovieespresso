package com.catalystdevworks.popularmoviepicker.helpers.MainActivity;

import com.catalystdevworks.popularmoviepicker.R;
import com.catalystdevworks.popularmoviepicker.helpers.BaseHelpers;

import static android.support.test.espresso.Espresso.onData;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static com.catalystdevworks.popularmoviepicker.constants.Constants.FAVORITES_MENU_TEXT;
import static com.catalystdevworks.popularmoviepicker.constants.Constants.LIST_VIEW_ID;
import static com.catalystdevworks.popularmoviepicker.constants.Constants.MOVIE_TITLE;
import static com.catalystdevworks.popularmoviepicker.constants.Constants.POPULAR_MENU_TEXT;
import static com.catalystdevworks.popularmoviepicker.constants.Constants.RATED_MENU_TEXT;
import static com.catalystdevworks.popularmoviepicker.constants.Constants.SEARCH_BUTTON_ID;
import static com.catalystdevworks.popularmoviepicker.constants.Constants.SEARCH_FIELD;
import static com.catalystdevworks.popularmoviepicker.constants.Constants.SUBMIT_SEARCH_BUTTON;
import static org.hamcrest.Matchers.anything;

/**
 * Created by gfisher on 2/22/2016.
 */
public class MainActivityHelpers extends BaseHelpers {

    public void searchForMovie(String text) {
        searchForMovieWithText(text);
    }

    public void ensureTitleOfFirstResult(String text) {
        ensureTextAppearsInNthRow(text, 1);
    }

    public void viewDetailsOfNthMovie(int position) {
        onData(anything())
                .inAdapterView(withId(LIST_VIEW_ID))
                .atPosition(0)
                .perform(click());
    }

    /**
     * Sorts movies by rating on the MainActivity
     */
    public void sortMoviesByRating() {
        openOverflowMenu();
        tapViewWithText(RATED_MENU_TEXT);
    }

    /**
     * Sorts movies by Popularity on the MainActivity
     */
    public void sortMoviesByPopularity() {
        openOverflowMenu();
        tapViewWithText(POPULAR_MENU_TEXT);
    }

    /**
     * Shows all favorited movies on the MainActivity
     */
    public void showFavoritedMovies() {
        openOverflowMenu();
        tapViewWithText(FAVORITES_MENU_TEXT);
    }

    /*public void searchForMovie(String movie) {
        enterTextToViewWithID(movie, );
    }*/

    public void searchForMovieWithText(String searchText) {
        onView(withId(SEARCH_BUTTON_ID)).perform(click());
        onView(withId(SEARCH_FIELD)).perform(typeText(searchText));
        onView(withId(SUBMIT_SEARCH_BUTTON)).perform(click());
    }

    public void ensureTextAppearsInNthRow(String text, int row) {
        onData(anything())
                .inAdapterView(withId(R.id.listview_movies))
                .atPosition(row - 1)
                .onChildView(withId(MOVIE_TITLE))
                .check(matches(withText(text)));
    }
}
