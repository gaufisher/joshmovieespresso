package com.catalystdevworks.popularmoviepicker.helpers.MainActivity;

import com.catalystdevworks.popularmoviepicker.helpers.BaseHelpers;

import static com.catalystdevworks.popularmoviepicker.constants.Constants.POPULAR_MENU_TEXT;

/**
 * This class contains methods specific to the main Activity when showing popular movies
 * Created by gfisher on 2/22/2016.
 */
public class MainActivityPopularHelpers extends BaseHelpers {



}
