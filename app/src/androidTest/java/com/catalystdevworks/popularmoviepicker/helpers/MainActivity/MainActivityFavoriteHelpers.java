package com.catalystdevworks.popularmoviepicker.helpers.MainActivity;

import static android.support.test.espresso.Espresso.onData;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static com.catalystdevworks.popularmoviepicker.constants.Constants.FAVORITE_BUTTON_ID;
import static com.catalystdevworks.popularmoviepicker.constants.Constants.LIST_VIEW_ID;
import static org.hamcrest.Matchers.anything;

/**
 * This class contains methods that are specific to the Main Activity when showing only favorited movies
 * Created by gfisher on 2/22/2016.
 */
public class MainActivityFavoriteHelpers extends BasePopularMovieHelpers {

    MainActivityHelpers mainActivity = new MainActivityHelpers();

    /**
     * Clicks the favorite button of the most popular movie.
     */
    public void favoriteMostPopularMovie() {
        mainActivity.sortMoviesByPopularity();
        onData(anything())
                .inAdapterView(withId(LIST_VIEW_ID))
                .atPosition(0)
                .onChildView(withId(FAVORITE_BUTTON_ID))
                .perform(click());
    }
}
