package com.catalystdevworks.popularmoviepicker.helpers;

import android.support.annotation.IdRes;
import android.support.test.InstrumentationRegistry;
import android.support.test.espresso.ViewInteraction;
import android.support.test.espresso.matcher.BoundedMatcher;
import android.support.v7.widget.Toolbar;

import com.catalystdevworks.popularmoviepicker.R;

import org.hamcrest.Description;
import org.hamcrest.Matcher;

import static android.support.test.espresso.Espresso.onData;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.Espresso.openActionBarOverflowOrOptionsMenu;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.scrollTo;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.hasDescendant;
import static android.support.test.espresso.matcher.ViewMatchers.isAssignableFrom;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withChild;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withParent;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.anything;
import static org.hamcrest.Matchers.is;

import static com.catalystdevworks.popularmoviepicker.constants.Constants.*;
/**
 * Created by gfisher on 2/10/2016.
 */
public class EspressoHelpers {

    /**
     * This method allows you to click on a view by text
     * @param text The visible text that you want to click on
     */
    public static void tapViewWithText(String text) {
        onView(withText(text)).perform(click());
    }

    /**
     * Opens the action bar overflow menu
     */
    public static void openOverflowMenu() {
        openActionBarOverflowOrOptionsMenu(InstrumentationRegistry.getTargetContext());
    }

    /**
     * Scrolls to a view with the given id
     * @param viewID R.id of the view you want to scroll to
     */
    public static void scrollToViewWithId(@IdRes int viewID) {
        onView(withId(viewID))
                .perform(scrollTo());
    }

    /**
     * Allows text to be entered into a view with a given R.id
     * @param textToEnter Text you would like to enter
     * @param viewID target view's R.id
     */
    public static void enterTextToViewWithID(String textToEnter, @IdRes int viewID) {
        onView(withId(viewID))
                .perform(typeText(textToEnter));
    }

    /**
     * performs a click on a view with the given id
     * @param viewID target view's R.id
     */
    public static void tapViewWithID(@IdRes int viewID) {
        onView(withId(viewID))
                .perform(click());
    }

    /**
     * Checks that the target view contains the string provided
     * @param expected String with expected text for view
     * @param viewID target view's R.id
     */
    public static void checkTextMatchesByID(String expected, @IdRes int viewID) {
        onView(withId(viewID))
                .check(matches(withText(expected)));
    }

    /**
     * Checks that the nth child row is visible
     * @param row The row number you expect is visible
     * @param viewID the R.id of the parent view to the row.
     */
    public static void checkNthRowVisibleInParentWithId(int row, @IdRes int viewID) {
        onData(anything())
                .inAdapterView(withId(viewID))
                .atPosition(row - 1)
                .check(matches(isDisplayed()));
    }

    public static void deleteDatabase() {
        InstrumentationRegistry.getTargetContext().getApplicationContext().deleteDatabase(DATABASE_NAME);
    }

    public static void sortMoviesByRating() {
        openOverflowMenu();
        tapViewWithText(RATED_MENU_TEXT);
    }

    public static void sortMoviesByPopularity() {
        openOverflowMenu();
        tapViewWithText(POPULAR_MENU_TEXT);
    }

    public static void showFavoritedMovies() {
        openOverflowMenu();
        tapViewWithText(FAVORITES_MENU_TEXT);
    }

    public static void viewDetailsOfNthMovie(int position) {
        onData(anything())
                .inAdapterView(withId(LIST_VIEW_ID))
                .atPosition(0)
                .perform(click());
    }

    /**
     * Clicks the favorite button of the first movie that shows up in the main activity.
     */
    public static void favoriteFirstMovie() {
        sortMoviesByPopularity();
        onData(anything())
                .inAdapterView(withId(LIST_VIEW_ID))
                .atPosition(0)
                .onChildView(withId(FAVORITE_BUTTON_ID))
                .perform(click());
    }

    public static void checkViewHasDescendantWithId(@IdRes int parentId, @IdRes int childId) {
        onView(withId(parentId))
                .check(matches(hasDescendant(withId(childId))));
    }

    /**
     * Checks that the toolbar title matches the expected title
     * @param title Expected toolbar title
     * @return the view interaction
     */
    public static ViewInteraction matchToolbarTitle(CharSequence title) {
        return onView(isAssignableFrom(Toolbar.class)).check(matches(withToolbarTitle(is(title))));
    }

    /**
     * matches the expected toolbar title with the actual toolbar title
     * @param textMatcher matcher of the expected toolbar title
     * @return matcher
     */
    private static Matcher<Object> withToolbarTitle(
            final Matcher<CharSequence> textMatcher) {
        return new BoundedMatcher<Object, Toolbar>(Toolbar.class) {
            @Override public boolean matchesSafely(Toolbar toolbar) {
                return textMatcher.matches(toolbar.getTitle());
            }
            @Override public void describeTo(Description description) {
                description.appendText("with toolbar title: ");
                textMatcher.describeTo(description);
            }
        };
    }



}
