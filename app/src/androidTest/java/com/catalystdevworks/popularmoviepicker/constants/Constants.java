package com.catalystdevworks.popularmoviepicker.constants;

import android.support.annotation.IdRes;

import com.catalystdevworks.popularmoviepicker.R;

/**
 * Created by gfisher on 2/10/2016.
 */
public class Constants {

    //OVERFLOW MENU ******************************************

    //Sort By Menu Text
    public static final String RATED_MENU_TEXT = "Top rated";
    public static final String POPULAR_MENU_TEXT = "Popular";
    public static final String FAVORITES_MENU_TEXT = "Favorites";
    public static final String AUTO_COMPLETE_TEXT = "Auto complete";


    //MAIN VIEW ************************************************

    public static final String APP_TITLE = "Popular Movie Picker";

    //Sorting By ________ Text
    public static final String POPULARITY_SORTING_TEXT = "Sorting by most popular";
    public static final String RATING_SORTING_TEXT = "Sorting by top rated";
    public static final String FAVORITES_SORTING_TEXT = "Showing your favorites";
    public static final String NO_FAVORITES_TEXT = "No favorites to show";

    //Search Entries
    public static final String VALID_SEARCH = "Moneyball";
    public static final String INVALID_SEARCH = "*&^^";

    //Searching Header Text
    public static final String SEARCH_HEADER_NO_RESULTS = "No results found!";
    public static final String SEARCH_HEADER_WITH_RESULTS = "Searching for " + VALID_SEARCH;

    //Search Results Text
    public static final String RATING = "Rating: ";


    //DETAILS VIEW ***********************************************
    public static final String DETAILS_TOOLBAR_TITLE = "Details";


    //VIEW IDS ***************************************************
    public static final @IdRes int SORT_BY_TEXT_ID= R.id.sort_by_text;
    public static final @IdRes int LIST_VIEW_ID = R.id.listview_movies;
    public static final @IdRes int FAVORITE_BUTTON_ID = R.id.favorite_icon;
    public static final @IdRes int LIST_VIEW_ROW_TEXT_CONTAINER_ID = R.id.text_container;
    public static final @IdRes int SEARCH_BUTTON_ID = R.id.search;
    public static final @IdRes int TOOLBAR_ID = R.id.toolbar;
    public static final @IdRes int SUBMIT_SEARCH_BUTTON = R.id.search_go_btn;
    public static final @IdRes int SEARCH_FIELD = R.id.search_src_text;
    public static final @IdRes int MOVIE_TITLE = R.id.title_textview;
    public static final @IdRes int MOVIE_RATING = R.id.rating_textview;

    //DATABASE NAME
    public static final String DATABASE_NAME = "favorite_movies";

}
