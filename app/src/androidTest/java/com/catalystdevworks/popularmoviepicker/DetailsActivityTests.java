package com.catalystdevworks.popularmoviepicker;

import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.test.suitebuilder.annotation.LargeTest;

import com.catalystdevworks.popularmoviepicker.activities.DetailsActivity;

import junit.framework.Assert;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static com.catalystdevworks.popularmoviepicker.constants.Constants.*;
import static com.catalystdevworks.popularmoviepicker.helpers.EspressoHelpers.matchToolbarTitle;

/**
 * Created by gfisher on 2/10/2016.
 */
@RunWith(AndroidJUnit4.class)
@LargeTest
public class DetailsActivityTests {

    @Rule
    public ActivityTestRule<DetailsActivity> mActivityRule = new ActivityTestRule<DetailsActivity>(
            DetailsActivity.class);

    @Test
    public void titleIsCorrect() {
        final String expectedText = DETAILS_TOOLBAR_TITLE;
        matchToolbarTitle(expectedText);
    }

}
