package com.catalystdevworks.popularmoviepicker;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.test.suitebuilder.annotation.LargeTest;

import com.catalystdevworks.popularmoviepicker.activities.MainActivity;
import com.catalystdevworks.popularmoviepicker.helpers.MainActivity.MainActivityHelpers;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onData;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static com.catalystdevworks.popularmoviepicker.helpers.EspressoHelpers.*;
import static com.catalystdevworks.popularmoviepicker.constants.Constants.*;
import static org.hamcrest.Matchers.anything;

/**
 * Created by gfisher on 2/9/2016.
 */
@RunWith(AndroidJUnit4.class)
@LargeTest
public class MainActivityTests extends MainActivityHelpers {

    @Rule
    public ActivityTestRule<MainActivity> mActivityRule = new ActivityTestRule<MainActivity>(
            MainActivity.class);

    /*
        This is why I use helper classes! The searchForFrozen() test does this same thing, but takes advantage of the helper classes I made.
     */

    /*
    @Test
    public void demoTest() {
        onView(withId(R.id.search)).perform(click()); //Clicks on the search button
        onView(withId(R.id.search_src_text)).perform(typeText("Star Wars")); //Enters text into the search field
        onView(withId(R.id.search_go_btn)).perform(click()); //Taps on the search button to execute search
        onData(anything())
                .inAdapterView(withId(R.id.listview_movies))
                .atPosition(0)
                .onChildView(withId(R.id.title_textview))
                .check(matches(withText("Star Wars")));
    }
    */


    @Test
    public void titleIsCorrect() {
        final String expectedText = APP_TITLE;
        matchToolbarTitle(expectedText);
    }


    @Test
    public void getDetailsFromMainActivity() {
        viewDetailsOfNthMovie(1);
        final String expectedText = DETAILS_TOOLBAR_TITLE;
        matchToolbarTitle(expectedText);
    }

    @Test
    public void searchForFrozen() {
        searchForMovie("Frozen");
        ensureTitleOfFirstResult("Frozen");
    }






}
