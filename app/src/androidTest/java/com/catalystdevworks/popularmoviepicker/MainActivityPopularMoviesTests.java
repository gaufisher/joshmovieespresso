package com.catalystdevworks.popularmoviepicker;

import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.test.suitebuilder.annotation.LargeTest;

import com.catalystdevworks.popularmoviepicker.activities.MainActivity;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static com.catalystdevworks.popularmoviepicker.constants.Constants.LIST_VIEW_ID;
import static com.catalystdevworks.popularmoviepicker.constants.Constants.LIST_VIEW_ROW_TEXT_CONTAINER_ID;
import static com.catalystdevworks.popularmoviepicker.constants.Constants.POPULARITY_SORTING_TEXT;
import static com.catalystdevworks.popularmoviepicker.constants.Constants.SORT_BY_TEXT_ID;
import static com.catalystdevworks.popularmoviepicker.helpers.EspressoHelpers.checkNthRowVisibleInParentWithId;
import static com.catalystdevworks.popularmoviepicker.helpers.EspressoHelpers.checkTextMatchesByID;
import static com.catalystdevworks.popularmoviepicker.helpers.EspressoHelpers.checkViewHasDescendantWithId;
import static com.catalystdevworks.popularmoviepicker.helpers.EspressoHelpers.sortMoviesByPopularity;

/**
 * Created by gfisher on 2/17/2016.
 */
@RunWith(AndroidJUnit4.class)
@LargeTest
public class MainActivityPopularMoviesTests {

    @Rule
    public ActivityTestRule<MainActivity> mActivityRule = new ActivityTestRule<MainActivity>(
            MainActivity.class);

    @Before
    public void setUp() {
        sortMoviesByPopularity();
    }

    @Test
    public void sortByPopularTextIsCorrect() {
        final String expectedText = POPULARITY_SORTING_TEXT;
        checkTextMatchesByID(expectedText, SORT_BY_TEXT_ID);
    }


    @Test
    public void ensureMoreThanNonePopularMovies() {
        checkViewHasDescendantWithId(LIST_VIEW_ID, LIST_VIEW_ROW_TEXT_CONTAINER_ID);
    }

    @Test
    public void ensureTwentyResultsVisibleWhenSortingByPopularity() {
        checkNthRowVisibleInParentWithId(20, LIST_VIEW_ID);
    }

}
