package com.catalystdevworks.popularmoviepicker.activities;

import static com.catalystdevworks.popularmoviepicker.Constants.*;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.catalystdevworks.popularmoviepicker.R;
import com.catalystdevworks.popularmoviepicker.entities.Movie;
import com.catalystdevworks.popularmoviepicker.fragments.DetailsActivityFragment;
import com.catalystdevworks.popularmoviepicker.fragments.MainActivityFragment;

import java.io.UnsupportedEncodingException;

public class MainActivity extends AppCompatActivity implements MainActivityFragment.OnMovieSelectedListener, SearchView.OnQueryTextListener, DetailsActivityFragment.OnFavoritedListener {

    private final String TAG = this.getClass().getSimpleName();
    private String sortBy;
    private boolean autocomplete;
    private DetailsActivityFragment detailsFragment;
    private SearchManager searchManager;
    private SearchView searchView;
    private SharedPreferences prefs;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        prefs = getSharedPreferences(SHARED_PREF_FILE, Context.MODE_PRIVATE);
        sortBy = prefs.getString(SORT_BY_SETTING, POPULAR_MOVIES);
        autocomplete = prefs.getBoolean(AUTOCOMPLETE_SETTING, true);
        detailsFragment = (DetailsActivityFragment) getSupportFragmentManager().findFragmentById(R.id.details_fragment);

    }

    @Override
    protected void onDestroy() {
        detailsFragment = null;
        super.onDestroy();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.menu_main, menu);

        searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        MenuItem searchMenuItem = menu.findItem(R.id.search);

        searchView = (SearchView) searchMenuItem.getActionView();
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        searchView.setSubmitButtonEnabled(true);
        searchView.setOnQueryTextListener(this);

        menu.findItem(R.id.auto_complete_setting).setChecked(autocomplete);

        switch (sortBy) {
            case POPULAR_MOVIES:
                menu.findItem(R.id.popular_setting).setChecked(true);
                ((TextView) findViewById(R.id.sort_by_text)).setText(getText(R.string.show_by_popular_string));
                break;
            case TOP_RATED_MOVIES:
                menu.findItem(R.id.top_rating_setting).setChecked(true);
                ((TextView) findViewById(R.id.sort_by_text)).setText(getText(R.string.show_by_top_rated_string));
                break;
            case FAVORITE_MOVIES:
                menu.findItem(R.id.favorite_setting).setChecked(true);
                break;
        }
        return true;
    }

    @Override
    public void onMovieSelected(Movie movie) {
        //check if we have both fragments loaded(on large screens) or just one(small screens) and load the movie
        if (detailsFragment == null || !detailsFragment.isInLayout()) {
            Intent detailsIntent = new Intent(this, DetailsActivity.class);
            detailsIntent.putExtra(PARCELABLE_MOVIE_KEY, movie);
            startActivity(detailsIntent);
        } else {
            detailsFragment.setMovie(movie);
        }
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        MainActivityFragment fragment = (MainActivityFragment) getSupportFragmentManager().findFragmentById(R.id.fragment);
        try {
            fragment.searchMovies(query);
            ((TextView) findViewById(R.id.sort_by_text)).setText(String.format(getString(R.string.show_by_searching), query));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        searchView.clearFocus();
        return true;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        if (!autocomplete) {
            return false;
        }

        MainActivityFragment fragment = (MainActivityFragment) getSupportFragmentManager().findFragmentById(R.id.fragment);

        //if search is empty don't try to search and get movies based on sort preference
        if (newText == null || newText.equals("")) {
            if (sortBy.equals(FAVORITE_MOVIES)) {
                fragment.getFavorites();
            } else {
                fragment.getJson(sortBy);
            }
            switch (sortBy) {
                case POPULAR_MOVIES:
                    ((TextView) findViewById(R.id.sort_by_text)).setText(getString(R.string.show_by_popular_string));
                    break;
                case TOP_RATED_MOVIES:
                    ((TextView) findViewById(R.id.sort_by_text)).setText(getString(R.string.show_by_top_rated_string));
                    break;
                case FAVORITE_MOVIES:
                    ((TextView) findViewById(R.id.sort_by_text)).setText(getString(R.string.show_by_favorites));
            }
            return true;
        }
        try {
            fragment.searchMovies(newText);
            ((TextView) findViewById(R.id.sort_by_text)).setText(String.format(getString(R.string.show_by_searching), newText));

        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.popular_setting:
                sortBy = POPULAR_MOVIES;
                break;
            case R.id.top_rating_setting:
                sortBy = TOP_RATED_MOVIES;
                break;
            case R.id.favorite_setting:
                sortBy = FAVORITE_MOVIES;
                break;
            case R.id.auto_complete_setting:
                autocomplete = !autocomplete;
                item.setChecked(!item.isChecked());
                prefs.edit().putBoolean(AUTOCOMPLETE_SETTING, autocomplete).commit();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onFavoriteClick() {
        MainActivityFragment fragment = (MainActivityFragment) getSupportFragmentManager().findFragmentById(R.id.fragment);
        fragment.getListView().invalidateViews();
    }
}