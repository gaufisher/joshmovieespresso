package com.catalystdevworks.popularmoviepicker.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;

import com.catalystdevworks.popularmoviepicker.R;
import com.catalystdevworks.popularmoviepicker.entities.Movie;
import com.catalystdevworks.popularmoviepicker.fragments.DetailsActivityFragment;

import static com.catalystdevworks.popularmoviepicker.Constants.*;

public class DetailsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        Intent intent = this.getIntent();
        Movie movie = intent.getParcelableExtra(PARCELABLE_MOVIE_KEY);
        ((DetailsActivityFragment) getSupportFragmentManager().findFragmentById(R.id.details_fragment)).setMovie(movie);
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_details, menu);
        return super.onCreateOptionsMenu(menu);
    }


}
