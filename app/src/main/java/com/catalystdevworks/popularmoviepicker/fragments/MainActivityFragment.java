package com.catalystdevworks.popularmoviepicker.fragments;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.catalystdevworks.popularmoviepicker.R;
import com.catalystdevworks.popularmoviepicker.adapters.MovieAdapter;
import com.catalystdevworks.popularmoviepicker.databases.MovieOpenHelper;
import com.catalystdevworks.popularmoviepicker.entities.Movie;
import com.catalystdevworks.popularmoviepicker.volley.VolleySingleton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import static com.catalystdevworks.popularmoviepicker.Constants.*;

public class MainActivityFragment extends Fragment {

    private final String LOG_TAG = this.getClass().getSimpleName();

    private View rootView;
    private OnMovieSelectedListener listener;
    private ListView listView;
    private MovieAdapter adapter;
    private List<Movie> resultList = new ArrayList<>();
    private JsonObjectRequest request;
    private JSONObject moviesJson;
    private SharedPreferences preferences;
    private String sortBy;
    //for getting favorite movies
    private int favoriteCount = -1;
    private int obtainedCount;

    public MainActivityFragment() {
    }

    public interface OnMovieSelectedListener {
        void onMovieSelected(Movie movie);
    }

    /*public interface OnFavoriteSelectedListener {
        void onFavoriteSelected();
    }*/

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        adapter = new MovieAdapter(getActivity(),
                R.layout.list_item_movie,
                new ArrayList<Movie>());
        if (context instanceof AppCompatActivity) {//not sure if this is necessary, someone on stackoverflow suggested it since onAttach(Activity) is deprecated
            try {
                listener = (OnMovieSelectedListener) context;
            } catch (ClassCastException e) {
                throw new ClassCastException(context.toString() + " must implement OnMovieSelectedListener");
            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        setHasOptionsMenu(true);
        rootView = inflater.inflate(R.layout.fragment_main, container, false);

        //get sort preference and load view based on it
        preferences = getActivity().getSharedPreferences(SHARED_PREF_FILE, Context.MODE_PRIVATE);
        sortBy = preferences.getString(SORT_BY_SETTING, POPULAR_MOVIES);
        if (sortBy.equals(FAVORITE_MOVIES)) {
            getFavorites();
        } else {
            getJson(sortBy);
        }
        listView = (ListView) rootView.findViewById(R.id.listview_movies);
        listView.setAdapter(adapter);
        return rootView;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    public void getFavorites() {
        resultList.clear();
        MovieOpenHelper movieOpenHelper = new MovieOpenHelper(getContext());
        favoriteCount = movieOpenHelper.getFavoritesCount();
        int[] favorites = movieOpenHelper.getFavorites();

        if (favorites.length < 1) {
            adapter.clear();
            ((TextView) rootView.findViewById(R.id.sort_by_text)).setText(getString(R.string.show_by_favorites_error));
            return;
        }
        //AHHHHHHHHHHHH ANDROID
        ((TextView) rootView.findViewById(R.id.sort_by_text)).setText(getText(R.string.show_by_favorites));
        for (int i : favorites) {
            getMovie(i);
        }
    }

    //calls themoviedb.org api using volley based on popular or top rated setting
    public void getJson(String sortBy) {
        request = new JsonObjectRequest(
                Request.Method.GET,
                BASE_MOVIE_API_URL + sortBy + "?" + API_PARAM,
                createSuccessListener(),
                createErrorListener());

        VolleySingleton.getInstance(getContext()).addToRequestQueue(request);
    }

    //adds request object for individual movie
    private void getMovie(int movieId) {
        JsonObjectRequest req = new JsonObjectRequest(
                Request.Method.GET,
                BASE_MOVIE_API_URL + movieId + "?" + API_PARAM,
                movieSuccessListener(),
                createErrorListener());

        VolleySingleton.getInstance(getContext()).addToRequestQueue(req);
    }

    //uses themoviedb.org search api to get movies
    public void searchMovies(String movieName) throws UnsupportedEncodingException {
        Log.d(LOG_TAG, BASE_SEARCH_URL + URLEncoder.encode(movieName, "UTF-8") + "&" + API_PARAM);
        request = new JsonObjectRequest(
                Request.Method.GET,
                BASE_SEARCH_URL + URLEncoder.encode(movieName, "UTF-8") + "&" + API_PARAM,
                createSuccessListener(),
                createErrorListener());

        VolleySingleton.getInstance(getContext()).addToRequestQueue(request);
    }

    //success callback for individual movie
    private Response.Listener<JSONObject> movieSuccessListener() {
        return new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    Movie movie = new Movie(response);
                    movie.setFavorite(true);
                    resultList.add(movie);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                //check if this is the last item and populate the ListView if it is
                if (++obtainedCount >= favoriteCount) { //yay incrementer in if statement
                    populateList();
                    //reset counters
                    favoriteCount = -1;
                    obtainedCount = 0;
                }
            }
        };
    }

    //success callback for list of popular/top rated movies
    private Response.Listener<JSONObject> createSuccessListener() {
        return new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                moviesJson = response;
                try {
                    parseJson();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                populateList();
            }
        };
    }

    private Response.ErrorListener createErrorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(LOG_TAG, "*******************");
                Log.d(LOG_TAG, "*******************");
                Log.d(LOG_TAG, "*******************");
                Log.d(LOG_TAG, error.toString());
                Log.d(LOG_TAG, "*******************");
                Log.d(LOG_TAG, "*******************");
                Log.d(LOG_TAG, "*******************");
            }
        };
    }

    private void parseJson() throws JSONException {

        JSONArray results;

        if (moviesJson != null) {
            results = moviesJson.getJSONArray("results");
            //toggle visibility of no results message if no results are returned
            if (results.length() < 1) {
                rootView.findViewById(R.id.sort_by_text).setVisibility(View.GONE);
                rootView.findViewById(R.id.no_results_text).setVisibility(View.VISIBLE);
            } else {
                rootView.findViewById(R.id.sort_by_text).setVisibility(View.VISIBLE);
                rootView.findViewById(R.id.no_results_text).setVisibility(View.GONE);
            }
        } else {
            Log.d(LOG_TAG, "Something went wrong");
            return;
        }

        if (resultList == null) {
            resultList = new ArrayList<>();
        } else {
            resultList.clear();
        }
        MovieOpenHelper movieOpenHelper = new MovieOpenHelper(getContext());

        for (int i = 0; i < results.length(); i++) {
            JSONObject jsonObject = results.getJSONObject(i);
            Movie result = new Movie(jsonObject);
            result.setFavorite(movieOpenHelper.isFavorite(result.getId()));
            resultList.add(result);
        }
    }

    private void populateList() {
        adapter.clear();
        adapter.addAll(resultList);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                listener.onMovieSelected(adapter.getItem(position));
            }
        });
    }

    public ListView getListView() {
        return this.listView;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.popular_setting:
                getJson(POPULAR_MOVIES);
                preferences.edit().putString(SORT_BY_SETTING, POPULAR_MOVIES).commit();
                ((TextView) rootView.findViewById(R.id.sort_by_text)).setText(getText(R.string.show_by_popular_string));
                item.setChecked(true);
                return true;
            case R.id.top_rating_setting:
                getJson(TOP_RATED_MOVIES);
                preferences.edit().putString(SORT_BY_SETTING, TOP_RATED_MOVIES).commit();
                ((TextView) rootView.findViewById(R.id.sort_by_text)).setText(getText(R.string.show_by_top_rated_string));
                item.setChecked(true);
                return true;
            case R.id.favorite_setting:
                ((TextView) rootView.findViewById(R.id.sort_by_text)).setText(getText(R.string.show_by_favorites));
                getFavorites();
                preferences.edit().putString(SORT_BY_SETTING, FAVORITE_MOVIES).commit();
                item.setChecked(true);
                return true;
            default:
                return false;
        }
    }
}