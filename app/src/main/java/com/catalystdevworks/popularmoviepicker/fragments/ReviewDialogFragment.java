package com.catalystdevworks.popularmoviepicker.fragments;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatDialogFragment;
import android.util.Log;
import android.widget.ArrayAdapter;

import com.catalystdevworks.popularmoviepicker.Constants;
import com.catalystdevworks.popularmoviepicker.R;
import com.catalystdevworks.popularmoviepicker.adapters.ReviewAdapter;
import com.catalystdevworks.popularmoviepicker.entities.Review;

import java.util.ArrayList;
import java.util.List;


public class ReviewDialogFragment extends AppCompatDialogFragment {

    private final String TAG = this.getClass().getSimpleName();

    List<Review> reviews;
    ReviewAdapter adapter;

    public ReviewDialogFragment() {
        // Required empty public constructor
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        adapter = new ReviewAdapter(
                getActivity(),
                R.layout.review_list_item,
                new ArrayList<Review>());

        reviews = getArguments().getParcelableArrayList(Constants.PARCELABLE_REVIEWS_KEY);

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(R.string.reviews_title)
                .setAdapter(adapter, null)
                .setPositiveButton(R.string.done_button, null);

        if (reviews != null) {
            populateList();
        } else {
            builder.setMessage("No reviews available");
        }

        return builder.create();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    private DialogInterface.OnClickListener setListener() {
        return new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        };
    }

    private void populateList() {
        if (reviews != null) {
            Log.d(TAG, "Populating review list");
            adapter.clear();
            adapter.addAll(reviews);
        } else {
            Log.d(TAG, "Review List appears to be null");
        }
    }
}
