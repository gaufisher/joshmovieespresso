package com.catalystdevworks.popularmoviepicker.fragments;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatDialogFragment;
import android.util.Log;
import android.widget.ArrayAdapter;

import com.catalystdevworks.popularmoviepicker.R;
import com.catalystdevworks.popularmoviepicker.entities.Trailer;

import java.util.ArrayList;
import java.util.List;

import static com.catalystdevworks.popularmoviepicker.Constants.*;

public class TrailerDialogFragment extends AppCompatDialogFragment {

    private final String TAG = this.getClass().getSimpleName();

    private List<Trailer> trailers;
    private ArrayAdapter<Trailer> adapter;

    public TrailerDialogFragment() {

    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        adapter = new ArrayAdapter<Trailer>(
                getActivity(),
                R.layout.trailer_list_item,
                R.id.trailer_item,
                new ArrayList<Trailer>());

        Bundle args = getArguments();
        trailers = args.getParcelableArrayList(PARCELABLE_TRAILERS_KEY);
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(R.string.trailers_title)
                .setAdapter(adapter, setListener())
                .setPositiveButton(R.string.done_button, null);
        if (trailers != null) {
            populateList();
        } else {
            builder.setMessage(getString(R.string.no_trailers_string));
        }
        return builder.create();
    }

    private DialogInterface.OnClickListener setListener() {
        return new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Log.d(TAG, "Clicked trailer: " + trailers.get(which).toString());
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse(trailers.get(which).getUrl()));
                startActivity(intent);
            }
        };
    }
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    public void setTrailers(List<Trailer> trailers) {
        this.trailers = trailers;
        populateList();
    }

    private void populateList() {
        if (trailers != null) {
            Log.d(TAG, "Adding trailers");
            adapter.clear();
            adapter.addAll(trailers);
        } else {
            Log.d(TAG, "List appears to be null");
        }
    }
}
