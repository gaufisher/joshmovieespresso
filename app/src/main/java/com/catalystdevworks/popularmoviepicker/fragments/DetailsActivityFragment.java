package com.catalystdevworks.popularmoviepicker.fragments;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.catalystdevworks.popularmoviepicker.R;
import com.catalystdevworks.popularmoviepicker.databases.MovieOpenHelper;
import com.catalystdevworks.popularmoviepicker.entities.Movie;
import com.catalystdevworks.popularmoviepicker.services.MovieService;
import com.catalystdevworks.popularmoviepicker.volley.VolleySingleton;

import java.util.ArrayList;

import uk.co.deanwild.flowtextview.FlowTextView;

import static com.catalystdevworks.popularmoviepicker.Constants.*;

public class DetailsActivityFragment extends Fragment {

    private final String LOG_TAG = this.getClass().getSimpleName();

    private View rootView;
    private Movie movie;
    private MovieOpenHelper movieOpenHelper;
    private OnFavoritedListener favoritedListener;

    public DetailsActivityFragment() {
    }

    public interface OnFavoritedListener {
        void onFavoriteClick();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        movieOpenHelper = new MovieOpenHelper(getContext());
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.fragment_details, container, false);

        if (savedInstanceState != null) {
            setMovie((Movie) savedInstanceState.getParcelable(PARCELABLE_MOVIE_KEY));
        }

        rootView.findViewById(R.id.favorite_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //I feel like making a db call onClick is bad but idk how else to do it
                if (movie.isFavorite()) {
                    movie.setFavorite(false);
                    movieOpenHelper.deleteFavorite(movie.getId());
                } else {
                    movie.setFavorite(true);
                    movieOpenHelper.addFavorite(movie.getId());
                }
                toggleStar();

                //still seems weird
                if (favoritedListener != null) {
                    favoritedListener.onFavoriteClick();
                }
            }
        });

        return rootView;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            favoritedListener = (OnFavoritedListener) getActivity();
        } catch (ClassCastException e) {
            favoritedListener = null;
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        if (movie != null) {
            outState.putParcelable(PARCELABLE_MOVIE_KEY, movie);
        }
        super.onSaveInstanceState(outState);
    }

    public void toggleStar() {
        if (movie == null) {
            return;
        }

        ImageView view = (ImageView) rootView.findViewById(R.id.favorite_button);
        if (movie.isFavorite()) {
            view.setImageResource(R.drawable.selected_star);
        } else {
            view.setImageResource(R.drawable.deselected_star);
        }
    }

    public void setMovie(Movie movie) {
        if (movie != null) {
            this.movie = movie;
            (new MovieService(this.movie)).getMovieTrailer();

            this.movie.getAdditionalInfo();
            displayMovieInfo();
        }
    }

    private void displayMovieInfo() {
        if (movie == null) {
            Log.d(LOG_TAG, "Movie is null");
            return;
        }

        movie.setFavorite(movieOpenHelper.isFavorite(movie.getId()));

        Log.d(LOG_TAG, movie.toString());

        //title
        ((TextView) rootView.findViewById(R.id.title)).setText(movie.getTitle());

        //favorite(Yay ternary
        ((ImageView) rootView.findViewById(R.id.favorite_button)).setImageResource(movie.isFavorite() ? R.drawable.selected_star : R.drawable.deselected_star);

        //rating
        ((TextView) rootView.findViewById(R.id.rating_text)).setVisibility(View.VISIBLE);
        RatingBar ratingBar = (RatingBar) rootView.findViewById(R.id.rating);
        ratingBar.setVisibility(View.VISIBLE);
        ratingBar.setRating(Float.valueOf(movie.getRating()) / 2);

        //release date
        ((TextView) rootView.findViewById(R.id.release_date)).setText(String.format(getString(R.string.release_date_string), movie.getReleaseDate()));

        //image
        ImageLoader imageLoader = VolleySingleton.getInstance(getContext()).getImageLoader();
        NetworkImageView networkImageView = (NetworkImageView) rootView.findViewById(R.id.image_poster);
        if (movie.getPosterUrl() != null) {
            networkImageView.setImageUrl(movie.getPosterUrl(), imageLoader);
        } else {
            networkImageView.setImageResource(R.drawable.noposter);
        }

        //details
        FlowTextView flowText = (FlowTextView) rootView.findViewById(R.id.details);
        flowText.setTextSize(getResources().getDisplayMetrics().scaledDensity * 14.0f); //have to manually set the size since it's hardcoded in the library
        flowText.setText(movie.getDetails());
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_reviews:
                ReviewDialogFragment reviewDialogFragment = new ReviewDialogFragment();
                Bundle reviewBundle = new Bundle();
                reviewBundle.putParcelableArrayList(PARCELABLE_REVIEWS_KEY, (ArrayList) movie.getReviews());
                reviewDialogFragment.setArguments(reviewBundle);
                reviewDialogFragment.show(getActivity().getSupportFragmentManager(), "REVIEW_FRAGMENT");
                return true;
            case R.id.action_trailers:
                TrailerDialogFragment trailerDialogFragment = new TrailerDialogFragment();
                Bundle trailerBundle = new Bundle();
                trailerBundle.putParcelableArrayList(PARCELABLE_TRAILERS_KEY, (ArrayList) movie.getTrailers());
                trailerDialogFragment.setArguments(trailerBundle);
                trailerDialogFragment.show(getActivity().getSupportFragmentManager(), "TRAILER_FRAGMENT");
                return true;
        }
        return false;
    }
}