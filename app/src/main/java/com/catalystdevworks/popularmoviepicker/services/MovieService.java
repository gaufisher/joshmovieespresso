package com.catalystdevworks.popularmoviepicker.services;

import android.util.Log;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.catalystdevworks.popularmoviepicker.entities.Movie;
import com.catalystdevworks.popularmoviepicker.entities.Review;
import com.catalystdevworks.popularmoviepicker.entities.Trailer;
import com.catalystdevworks.popularmoviepicker.volley.VolleySingleton;

import org.json.JSONObject;

import java.util.List;

import static com.catalystdevworks.popularmoviepicker.Constants.API_PARAM;
import static com.catalystdevworks.popularmoviepicker.Constants.BASE_MOVIE_API_URL;
import static com.catalystdevworks.popularmoviepicker.Constants.MOVIE_REVIEWS;
import static com.catalystdevworks.popularmoviepicker.Constants.MOVIE_TRAILERS;

public class MovieService {

    private final String TAG = this.getClass().getSimpleName();

    private Movie movie;

    public MovieService(Movie movie) {
        if (movie != null) {
            this.movie = movie;
        }
    }

    public void getMovieReviews() {
        JsonObjectRequest request = new JsonObjectRequest(
                Request.Method.GET,
                BASE_MOVIE_API_URL + movie.getId() + MOVIE_REVIEWS + "?" + API_PARAM,
                createReviewSuccessListener(),
                createReviewErrorListener());

        VolleySingleton.getInstance(null).addToRequestQueue(request);
    }

    public void getMovieTrailer() {
        JsonObjectRequest request = new JsonObjectRequest(
                Request.Method.GET,
                BASE_MOVIE_API_URL + movie.getId() + MOVIE_TRAILERS + "?" + API_PARAM,
                createTrailerSuccessListener(),
                createTrailerErrorListener());

        VolleySingleton.getInstance(null).addToRequestQueue(request);
    }

    private Response.Listener<JSONObject> createTrailerSuccessListener() {
        return new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Results<Trailer> results = new Results<>();
                List<Trailer> trailers = results.getResults(response, Trailer.class);
                movie.setTrailers(trailers);
            }
        };
    }

    private Response.ErrorListener createTrailerErrorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(TAG, "Error trying to get trailers for movie: " + movie.getTitle());
                movie.setTrailers(null);
            }
        };
    }

    private Response.Listener<JSONObject> createReviewSuccessListener() {
        return new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Results<Review> results = new Results<>();
                List<Review> reviews = results.getResults(response, Review.class);
                movie.setReviews(reviews);
            }
        };
    }

    private Response.ErrorListener createReviewErrorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(TAG, "Error getting reviews for movie: " + movie.getTitle());
                movie.setReviews(null);
            }
        };
    }
}
