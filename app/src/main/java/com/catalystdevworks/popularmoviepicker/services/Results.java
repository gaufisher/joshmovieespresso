package com.catalystdevworks.popularmoviepicker.services;

import android.util.Log;

import com.catalystdevworks.popularmoviepicker.entities.MovieDBObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class Results<T extends MovieDBObject> {

    private final String TAG = this.getClass().getSimpleName();

    //get the results back from a moviedb api call with a results array in the json
    public List<T> getResults(JSONObject json, Class<T> tClass) {
        JSONArray arr;

        try {
            arr = json.getJSONArray("results");

            if (arr.length() < 1) {
                Log.d(TAG, "No results found for " + tClass.getSimpleName() + "s");
                return null;
            }

            List<T> results = new ArrayList<>();
            for (int i = 0; i < arr.length(); i++) {
                Object obj = tClass.newInstance();
                ((T) obj).setFromJson(arr.getJSONObject(i));
                results.add((T) obj);
            }

            return results;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        } catch (InstantiationException e) {
            e.printStackTrace();
            return null;
        } catch (IllegalAccessException e) {
            e.printStackTrace();
            return null;
        }
    }
}
