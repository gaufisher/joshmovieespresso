package com.catalystdevworks.popularmoviepicker;


//holds constants for things only used on the Java side
public class Constants {

    //terrible way to store api keys but will work for now
    public static final String API_KEY = "c86aa96b7cfc7ea89f974756beb73760";

    //api constants
    public static final String BASE_IMAGE_URL = "https://image.tmdb.org/t/p/w185/";
    public static final String BASE_API_URL = "https://api.themoviedb.org/3/";
    public static final String BASE_MOVIE_API_URL = BASE_API_URL + "movie/";
    public static final String BASE_SEARCH_URL = "https://api.themoviedb.org/3/search/movie?query=";
    public static final String API_PARAM = "api_key=" + API_KEY;
    public static final String POPULAR_MOVIES = "popular";
    public static final String TOP_RATED_MOVIES = "top_rated";
    public static final String FAVORITE_MOVIES = "favorites";
    public static final String MOVIE_REVIEWS = "/reviews";
    public static final String MOVIE_TRAILERS = "/videos";

    //trailer urls
    public static final String BASE_YOUTUBE_URL = "https://www.youtube.com/watch?v=";

    //shared prefs constants
    public static final String SHARED_PREF_FILE = "com.catalystdevworks.popularmoviepicker.settings";
    public static final String SORT_BY_SETTING = "sortBy";
    public static final String AUTOCOMPLETE_SETTING = "autocomplete";

    //parcelable keys
    public static final String PARCELABLE_MOVIE_KEY = "MOVIE_KEY";
    public static final String PARCELABLE_TRAILERS_KEY = "TRAILERS_KEY";
    public static final String PARCELABLE_REVIEWS_KEY = "REVIEWS_KEY";
}
