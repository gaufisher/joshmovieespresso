package com.catalystdevworks.popularmoviepicker.entities;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.catalystdevworks.popularmoviepicker.Constants;
import com.catalystdevworks.popularmoviepicker.services.MovieService;
import com.catalystdevworks.popularmoviepicker.services.Results;
import com.catalystdevworks.popularmoviepicker.volley.VolleySingleton;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import static com.catalystdevworks.popularmoviepicker.Constants.*;

public class Movie extends MovieDBObject implements Parcelable {

    private final String TAG = this.getClass().getSimpleName();

    private String title;
    private String rating;
    private Integer id;
    private String details;
    private String releaseDate;
    private String posterUrl;
    private List<Review> reviews;
    private List<Trailer> trailers;
    private boolean favorite = false;

    public Movie(JSONObject movie) throws JSONException {
        setFromJson(movie);
    }

    public void getAdditionalInfo() {
        MovieService service = new MovieService(this);
        service.getMovieReviews();
        service.getMovieTrailer();
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public boolean isFavorite() {
        return favorite;
    }

    public void setFavorite(boolean favorite) {
        this.favorite = favorite;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public Integer getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public String getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(String releaseDate) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        Log.d("DATE", releaseDate);
        try {
            Date date = format.parse(releaseDate);
            Log.d("DATE", date.toString());

            format = new SimpleDateFormat("MMM dd, yyyy");
            this.releaseDate = format.format(date);
        } catch (ParseException e) {
            this.releaseDate = "Error";
            e.printStackTrace();
        }
    }

    public String getPosterUrl() {
        return posterUrl;
    }

    public void setPosterUrl(String posterUrl) {
        this.posterUrl = posterUrl;
    }

    public List<Review> getReviews() {
        return reviews;
    }

    public void setReviews(List<Review> reviews) {
        this.reviews = reviews;
    }

    public List<Trailer> getTrailers() {
        return trailers;
    }

    public void setTrailers(List<Trailer> trailers) {
        this.trailers = trailers;
    }

    public void setFromJson(JSONObject movie) throws JSONException {
        if (movie != null) {
            setTitle(movie.getString("title"));
            setRating(movie.getString("vote_average"));
            setId(movie.getInt("id"));
            setReleaseDate(movie.getString("release_date"));
            setDetails(movie.getString("overview"));
            String posterUrl = movie.getString("poster_path");
            //probably not necessary to check for null or "" as getString always returns a string but it'll be fine
            if (posterUrl != null && !posterUrl.equals("null") && !posterUrl.equals("")) {
                setPosterUrl(Constants.BASE_IMAGE_URL + posterUrl.replaceAll("^[\\\\/]", ""));
            }
        }
    }

    @Override
    public String toString() {
        return "Movie{" +
                "title='" + title + '\'' +
                ", rating='" + rating + '\'' +
                ", id=" + id +
                ", details='" + details + '\'' +
                ", releaseDate='" + releaseDate + '\'' +
                ", posterUrl='" + posterUrl + '\'' +
                ", reviews=" + reviews +
                ", trailers=" + trailers +
                ", favorite=" + favorite +
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.title);
        dest.writeString(this.rating);
        dest.writeValue(this.id);
        dest.writeString(this.details);
        dest.writeString(this.releaseDate);
        dest.writeString(this.posterUrl);
        dest.writeTypedList(reviews);
        dest.writeTypedList(trailers);
        dest.writeByte(favorite ? (byte) 1 : (byte) 0);
    }

    protected Movie(Parcel in) {
        this.title = in.readString();
        this.rating = in.readString();
        this.id = (Integer) in.readValue(Integer.class.getClassLoader());
        this.details = in.readString();
        this.releaseDate = in.readString();
        this.posterUrl = in.readString();
        this.reviews = in.createTypedArrayList(Review.CREATOR);
        this.trailers = in.createTypedArrayList(Trailer.CREATOR);
        this.favorite = in.readByte() != 0;
    }

    public static final Creator<Movie> CREATOR = new Creator<Movie>() {
        public Movie createFromParcel(Parcel source) {
            return new Movie(source);
        }

        public Movie[] newArray(int size) {
            return new Movie[size];
        }
    };
}
