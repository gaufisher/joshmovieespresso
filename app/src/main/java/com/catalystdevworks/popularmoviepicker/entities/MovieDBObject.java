package com.catalystdevworks.popularmoviepicker.entities;

import android.os.Parcelable;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

public abstract class MovieDBObject implements Parcelable {

    public MovieDBObject() {

    }

    public MovieDBObject(JSONObject json) throws JSONException {
        setFromJson(json);
    }

    public abstract void setFromJson(JSONObject json) throws JSONException;

}
