package com.catalystdevworks.popularmoviepicker.entities;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

public class Review extends MovieDBObject implements Parcelable {

    private final String TAG = this.getClass().getSimpleName();

    private String id;
    private String author;
    private String content;

    public Review() {
    }

    public Review(JSONObject json) throws JSONException {
        setFromJson(json);
    }

    public void setFromJson(JSONObject json) throws JSONException {
        if (json != null) {
            id = json.getString("id");
            author = json.getString("author");
            content = json.getString("content");
        } else {
            Log.d(TAG, "Review JSON is null");
        }
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Override
    public String toString() {
        return "Author: " + author +
                "\n" + content;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
        dest.writeString(this.author);
        dest.writeString(this.content);
    }

    protected Review(Parcel in) {
        this.id = in.readString();
        this.author = in.readString();
        this.content = in.readString();
    }

    public static final Creator<Review> CREATOR = new Creator<Review>() {
        public Review createFromParcel(Parcel source) {
            return new Review(source);
        }

        public Review[] newArray(int size) {
            return new Review[size];
        }
    };
}
