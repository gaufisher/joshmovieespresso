package com.catalystdevworks.popularmoviepicker.entities;

import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONException;
import org.json.JSONObject;

import static com.catalystdevworks.popularmoviepicker.Constants.*;

public class Trailer extends MovieDBObject implements Parcelable {

    private String id;
    private String name;
    private String url;
    private String site;
    private String language;

    public Trailer(JSONObject json) throws JSONException {
        setFromJson(json);
    }

    public void setFromJson(JSONObject json) throws JSONException {
        if (json != null) {
            id = json.getString("id");
            name = json.getString("name");
            site = json.getString("site");
            url = BASE_YOUTUBE_URL + json.getString("key");
            language = json.getString("iso_639_1");
        }
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getSite() {
        return site;
    }

    public void setSite(String site) {
        this.site = site;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
        dest.writeString(this.name);
        dest.writeString(this.url);
        dest.writeString(this.site);
        dest.writeString(this.language);
    }

    public Trailer() {
    }

    protected Trailer(Parcel in) {
        this.id = in.readString();
        this.name = in.readString();
        this.url = in.readString();
        this.site = in.readString();
        this.language = in.readString();
    }

    public static final Creator<Trailer> CREATOR = new Creator<Trailer>() {
        public Trailer createFromParcel(Parcel source) {
            return new Trailer(source);
        }

        public Trailer[] newArray(int size) {
            return new Trailer[size];
        }
    };

    @Override
    public String toString() {
        return name;
    }
}
