package com.catalystdevworks.popularmoviepicker.adapters;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.toolbox.NetworkImageView;
import static com.catalystdevworks.popularmoviepicker.Constants.*;
import com.catalystdevworks.popularmoviepicker.fragments.DetailsActivityFragment;
import com.catalystdevworks.popularmoviepicker.activities.MainActivity;
import com.catalystdevworks.popularmoviepicker.fragments.MainActivityFragment;
import com.catalystdevworks.popularmoviepicker.R;
import com.catalystdevworks.popularmoviepicker.databases.MovieOpenHelper;
import com.catalystdevworks.popularmoviepicker.entities.Movie;
import com.catalystdevworks.popularmoviepicker.volley.VolleySingleton;

import java.util.List;

public class MovieAdapter extends ArrayAdapter<Movie> {

    private final String TAG = this.getClass().getSimpleName();

    private Context context;
    private int layoutResourceId;
    private List<Movie> movies;

    public MovieAdapter(Context context, int resource, List<Movie> movies) {
        super(context, resource, movies);
        this.context = context;
        this.layoutResourceId = resource;
        this.movies = movies;
    }

    @Override
    public View getView(int position, View convertView, final ViewGroup parent) {
        View row = convertView;
        MovieHolder holder = null;
        final MovieOpenHelper movieOpenHelper = new MovieOpenHelper(getContext());

        final DetailsActivityFragment detailsFragment = (DetailsActivityFragment) ((MainActivity) getContext()).getSupportFragmentManager().findFragmentById(R.id.details_fragment);
        final MainActivityFragment mainFragment = (MainActivityFragment) ((MainActivityFragment) ((MainActivity) getContext()).getSupportFragmentManager().findFragmentById(R.id.fragment));

        if (row == null) {
            LayoutInflater inflater = ((Activity)context).getLayoutInflater();
            row = inflater.inflate(layoutResourceId, parent, false);

            holder = new MovieHolder();
            holder.imgIcon = (NetworkImageView)row.findViewById(R.id.poster_icon);
            holder.txtTitle = (TextView)row.findViewById(R.id.title_textview);
            holder.txtRating = (TextView)row.findViewById(R.id.rating_textview);
            holder.favoriteIcon = (ImageView) row.findViewById(R.id.favorite_icon);
            row.setTag(holder);
        } else {
            holder = (MovieHolder) row.getTag();
        }

        final Movie movie = movies.get(position);

        holder.txtTitle.setText(movie.getTitle());
        holder.txtRating.setText(String.format(getContext().getString(R.string.list_rating_string), movie.getRating()));

        final boolean favoriteSort = getContext().getSharedPreferences(SHARED_PREF_FILE, Context.MODE_PRIVATE)
                .getString(SORT_BY_SETTING, POPULAR_MOVIES)
                .equals(FAVORITE_MOVIES);
        if (movie.isFavorite()) {
            holder.favoriteIcon.setImageResource(R.drawable.selected_star); //.setVisibility(View.VISIBLE);
        } else {
            holder.favoriteIcon.setImageResource(R.drawable.deselected_star); //.setVisibility(View.INVISIBLE);
        }
        holder.favoriteIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (movie.isFavorite()) {
                    ((ImageView) v).setImageResource(R.drawable.deselected_star);
                    movieOpenHelper.deleteFavorite(movie.getId());
                    movie.setFavorite(false);
                } else {
                    ((ImageView) v).setImageResource(R.drawable.selected_star);
                    movieOpenHelper.addFavorite(movie.getId());
                    movie.setFavorite(true);
                }
                if (detailsFragment != null) {
                    detailsFragment.toggleStar();
                }
                if (favoriteSort) {
                    Log.d(TAG, "Should be removing from view now");
                    movies.remove(movie);
                    mainFragment.getListView().invalidateViews();
                }
            }
        });

        if (movie.getPosterUrl() != null) {
            holder.imgIcon.setImageUrl(movie.getPosterUrl(), VolleySingleton.getInstance(getContext()).getImageLoader());
        } else {
            holder.imgIcon.setImageResource(R.drawable.noposter);
        }

        return row;
    }

    //idk why this exists rather than calling directly but that's what happens when you follow internet tuts
    private static class MovieHolder {
        NetworkImageView imgIcon;
        TextView txtTitle;
        TextView txtRating;
        ImageView favoriteIcon;
    }
}
