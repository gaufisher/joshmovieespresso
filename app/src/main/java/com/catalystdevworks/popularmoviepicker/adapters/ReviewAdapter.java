package com.catalystdevworks.popularmoviepicker.adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.catalystdevworks.popularmoviepicker.R;
import com.catalystdevworks.popularmoviepicker.entities.Review;

import java.util.List;

public class ReviewAdapter extends ArrayAdapter<Review> {

    private Context context;
    private int layoutResourceId;
    private List<Review> reviews;

    public ReviewAdapter(Context context, int resource, List<Review> reviews) {
        super(context, resource, reviews);
        this.context = context;
        this.layoutResourceId = resource;
        this.reviews = reviews;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        ReviewHolder holder = null;

        if (row == null) {
            LayoutInflater inflater = ((Activity)context).getLayoutInflater();
            row = inflater.inflate(layoutResourceId, parent, false);

            holder = new ReviewHolder();
            holder.txtAuthor = (TextView) row.findViewById(R.id.review_author_textview);
            holder.txtContent = (TextView) row.findViewById(R.id.review_content_textview);
            row.setTag(holder);
        } else {
            holder = (ReviewHolder) row.getTag();
        }

        final Review review = reviews.get(position);

        holder.txtAuthor.setText(String.format(getContext().getString(R.string.author_text), review.getAuthor()));
        holder.txtContent.setText(review.getContent());

        return row;
    }

    //idk why this exists rather than calling directly but that's what happens when you follow internet tuts
    private static class ReviewHolder {
        TextView txtAuthor;
        TextView txtContent;
    }
}
