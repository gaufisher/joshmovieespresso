package com.catalystdevworks.popularmoviepicker.databases;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.text.DateFormat;
import java.util.Date;

import static com.catalystdevworks.popularmoviepicker.databases.MovieContract.*;

public class MovieOpenHelper extends SQLiteOpenHelper {

    private final String TAG = this.getClass().getSimpleName();

    private static final String DB_NAME = "favorite_movies";
    private static final int VERSION = 1;

    public MovieOpenHelper(Context context) {
        super(context, DB_NAME, null, VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(MOVIE_TABLE_CREATE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE " + MovieEntry.TABLE_NAME);
        onCreate(db);
    }

    @Override
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db, oldVersion, newVersion);
    }

    public boolean isFavorite(int movieId) {
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(
                MovieContract.MovieEntry.TABLE_NAME,
                new String[]{MovieEntry._ID},
                MovieEntry._ID + " = ?",
                new String[]{String.valueOf(movieId)},
                null,
                null,
                null);

        cursor.moveToFirst();
        int count = cursor.getCount();
        cursor.close();

        Log.d(TAG, String.valueOf(count));
        return count > 0;
    }

    public void addFavorite(int movieId) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues value = new ContentValues();
        value.put(MovieEntry._ID, movieId);
        //dates in java suck
        DateFormat format = DateFormat.getDateInstance();
        value.put(MovieEntry.DATE_FAVORITED, format.format(new Date()));
        Log.d(TAG, value.get(MovieEntry.DATE_FAVORITED).toString());

        db.insert(MovieEntry.TABLE_NAME, null, value);
    }

    public void deleteFavorite(int movieId) {
        SQLiteDatabase db = this.getWritableDatabase();

        db.delete(MovieEntry.TABLE_NAME, MovieEntry._ID + " = ?", new String[]{String.valueOf(movieId)});
    }

    public void dropFavorites() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.openDatabase("favorite_movies", null, SQLiteDatabase.OPEN_READWRITE).execSQL("DROP TABLE " + "movies");
    }

    public int getFavoritesCount() {
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(
                MovieContract.MovieEntry.TABLE_NAME,
                new String[]{MovieEntry._ID},
                null,
                null,
                null,
                null,
                null);

        cursor.moveToFirst();
        int count = cursor.getCount();
        cursor.close();
        Log.d(TAG, "Count is: " + String.valueOf(count));
        return count;
    }

    public int[] getFavorites() {
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(
                MovieContract.MovieEntry.TABLE_NAME,
                new String[]{MovieEntry._ID},
                null,
                null,
                null,
                null,
                null);
        int count = cursor.getCount();
        int[] movies = new int[count];

        //column index for _id
        int index = cursor.getColumnIndex(MovieEntry._ID);
        cursor.moveToFirst();
        for (int i = 0; i < count; i++) {
            movies[i] = cursor.getInt(index);
            cursor.moveToNext();
            Log.d(TAG, String.valueOf(movies[i]));

        }

        return movies;
    }


}
