package com.catalystdevworks.popularmoviepicker.databases;

import android.provider.BaseColumns;

import static com.catalystdevworks.popularmoviepicker.databases.MovieContract.MovieEntry.DATE_FAVORITED;
import static com.catalystdevworks.popularmoviepicker.databases.MovieContract.MovieEntry.TABLE_NAME;

public final class MovieContract {

    public MovieContract() {
        //empty constructor
    }

    public static abstract class MovieEntry implements BaseColumns {
        public static final String TABLE_NAME = "movies";
        public static final String DATE_FAVORITED = "date_favorited";
    }

    public static final String MOVIE_TABLE_CREATE =
            "CREATE TABLE " + TABLE_NAME + " (" +
                    MovieEntry._ID + " INTEGER PRIMARY KEY, " +
                    DATE_FAVORITED + " TEXT NOT NULL);";
}
