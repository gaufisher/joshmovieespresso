package com.catalystdevworks.popularmoviepicker;

import android.support.test.runner.AndroidJUnit4;
import android.test.suitebuilder.annotation.SmallTest;



/**
 * To work on unit tests, switch the Test Artifact in the Build Variants view.
 */
@RunWith(AndroidJunit4.class)
@SmallTest
public class ExampleUnitTest {


    public void addition_isCorrect() throws Exception {
        assertEquals(4, 2 + 2);
    }
}